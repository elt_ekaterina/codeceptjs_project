const { I } = inject();

module.exports = {
    button: {
        wallpapersMenuItem: '[href="/t/wallpapers"]',
        openPhoto: '[data-test="photo-grid-multi-col-figure"]',
        downloadPhoto: '[title="Download photo"]',
        likePhoto: '[title="Like photo"]',
        closePhoto: '//body/div[4]/div[1]/div[1]/div[1]/button[1]'
    },
    popups: {
        thank: '[data-test="say-thanks-card-sponsor-message"]',
    },
    fields: {
        search: '[data-test="nav-bar-search-form-input"]',
        header: 'h1',
    },

    async goToWallpapersListing() {
        await I.click(this.button.wallpapersMenuItem);
    },
    async openPhoto() {
        await I.click(this.button.openPhoto);
    },
    async downloadPhoto() {
        await I.click(this.button.downloadPhoto);
        await I.seeElement(this.popups.thank);
    },
    async startSearch(query) {
        await I.click(this.fields.search);
        await I.fillField(this.fields.search, query);
        await I.pressKey('Enter');
    },
    async getHeaderName() {
        const headerText = await I.grabTextFrom(this.fields.header);
        return headerText
    },
    async likePhoto() {
        await I.click(this.button.likePhoto);
    },
    async closePhotoPopup() {
        await I.click(this.button.closePhoto);
    }
}
