const { I } = inject();

module.exports = {
    button: {
        login: '[href="/login"]',
        submit: '[type="submit"]',
        personalMenu: '#popover-avatar-loggedin-menu-desktop',
        logout: '[href="https://unsplash.com/logout"]'
    },
    fields: {
        email: '#user_email',
        password: '#user_password',
    },

    async login(email, password) {
        await I.click(this.button.login)
        await I.click(this.fields.email);
        await I.fillField(this.fields.email, email);
        await I.click(this.fields.password);
        await I.fillField(this.fields.password, password);
        await I.click(this.button.submit);
    },
    async logout() {
        await I.click(this.button.personalMenu);
        await I.click(this.button.logout);
    }
}
