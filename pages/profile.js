const { I } = inject();

module.exports = {
    button: {
        personalMenu: '#popover-avatar-loggedin-menu-desktop',
        viewProfile: '//a[contains(text(),\'View profile\')]',
    },
    fields: {
        name: '[class="dJnu9 CjK9V Fu4vG lP2EO"]',
        likesCount: '//body/div[@id=\'app\']/div[1]/div[2]/div[3]/div[1]/div[1]/div[1]/div[1]/ul[1]/li[2]/a[1]/span[2]/span[1]\n',
        photo: '[data-test="photo-grid-multi-col-figure"]'
    },
    tabs: {
        like: '[data-test="user-nav-link-likes"]'
    },

    async goToProfile() {
        await I.click(this.button.personalMenu)
        await I.click(this.button.viewProfile);
    },
    async getUserName() {
        const userNameText = await I.grabTextFrom(this.fields.name);
        return userNameText
    },
    async goToLikeTab() {
        await I.click(this.tabs.like);
    },
    async getLikesNumber() {
        const likesNumberText = await I.grabTextFrom(this.fields.likesCount);
        return likesNumberText
    },
    async mouseOverPhoto() {
        await I.scrollTo(this.fields.photo);
        await I.moveCursorTo(this.fields.photo);
    }
}
