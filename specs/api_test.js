const assert = require('assert');

Feature('API tests');

Scenario('API Get a single page from the Editorial feed', async ({ I }) => {
    const res = await I.sendGetRequest('/photos');
    assert.deepEqual(res.status, 200)
});

Scenario('API Retrieve a single random photo', async ({ I }) => {
    const res = await I.sendGetRequest('/photos/random');
    assert.deepEqual(res.status, 200)
});

Scenario('API Search photos by "Patterns" query', async ({ I }) => {
    const query = 'patterns'
    const res = await I.sendGetRequest(`/search/photos?query=${query}`);
    assert.deepEqual(res.status, 200)
});

Scenario('API Retrieve public details on a given user', async ({ I }) => {
    const username = 'marvelous';
    const res = await I.sendGetRequest(`/users/${username}`);
    assert.deepEqual(res.status, 200)
});

Scenario('API Retrieve a topic’s photos', async ({ I }) => {
    const topic = 'holidays';
    const res = await I.sendGetRequest(`/topics/${topic}/photos`);
    assert.deepEqual(res.status, 200)
});
