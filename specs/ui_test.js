const assert = require('assert');
const email = '$EMAIL';
const password = '$PASSWORD';

Feature('Ui tests');

Scenario(' UI Login test', async ({ I, loginPage, profilePage }) => {
    I.amOnPage();
    await loginPage.login(email, password);
    await profilePage.goToProfile();
    const userNameText = await profilePage.getUserName();
    assert.deepEqual(userNameText, 'Ekaterina Eltysheva')
});

Scenario('UI See and download wallpapers photos test', async ({ I, loginPage, listingPage }) => {
    I.amOnPage('/');
    await loginPage.login(email, password);
    await listingPage.goToWallpapersListing();
    await listingPage.openPhoto();
    await listingPage.downloadPhoto();
});

Scenario('UI Search photos test', async ({ I, listingPage }) => {
    I.amOnPage();
    const query = 'trees';
    await listingPage.startSearch(query);
    const resultsName = await listingPage.getHeaderName();
    assert.deepEqual(resultsName, query.charAt(0).toUpperCase() + query.slice(1));
});

Scenario('UI Like and unlike photo test', async ({ I, loginPage, listingPage, profilePage }) => {
    I.amOnPage();
    await loginPage.login(email, password);
    await listingPage.openPhoto();
    await listingPage.likePhoto();
    await listingPage.closePhotoPopup();
    await profilePage.goToProfile();
    await I.refreshPage();
    await profilePage.goToLikeTab();
    await profilePage.mouseOverPhoto();
    await listingPage.likePhoto();
    await I.refreshPage();
    await I.wait(2);
    const likesNumber = await profilePage.getLikesNumber();
    assert.deepEqual(likesNumber, '0');

});

Scenario('UI Logout test', async ({ I, loginPage }) => {
    I.amOnPage();
    await loginPage.login(email, password);
    await loginPage.logout();
    await I.see('Log in');
});
