const { setHeadlessWhen } = require('@codeceptjs/configure');

// turn on headless mode when running with HEADLESS=true environment variable
// export HEADLESS=true && npx codeceptjs run
setHeadlessWhen(process.env.HEADLESS);

exports.config = {
  tests: './specs/*_test.js',
  output: './output',
  helpers: {
    Playwright: {
      url: 'https://unsplash.com/',
      show: false,
      browser: 'chromium',
      waitForAction: 500
    },
    REST: {
      endpoint: 'https://api.unsplash.com',
      defaultHeaders: {
        'Authorization': 'Client-ID CNy7SP_hpGDVgZuHbQZuSSKH0v_G2ZLpRETj2YzEnP4',
      }
      }
  },
  include: {
    I: './steps_file.js',
    listingPage: './pages/listings.js',
    loginPage: './pages/login.js',
    profilePage: './pages/profile.js',
  },
  bootstrap: null,
  mocha: {
    "reporterOptions": {
      "reportDir": "output"
    }
  },
  name: 'codeceptjs_project',
  plugins: {
    pauseOnFail: {},
    retryFailedStep: {
      enabled: true
    },
    tryTo: {
      enabled: true
    },
    screenshotOnFail: {
      enabled: true
    }
  }
}
